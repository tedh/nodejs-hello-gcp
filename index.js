require('@google-cloud/debug-agent').start({ allowExpressions: true });

var express = require('express');
var app = express();

var port = process.env.PORT || 3000;

app.get('/health', function (req, res) {
    res.send({status: 'OK'});
});

app.use('/', express.static('./public'));

app.listen(port, function (err) {
    if (err) {
        return log.info('Error running node app:', err);
    }
    console.log('node listening on ' + port);
});
